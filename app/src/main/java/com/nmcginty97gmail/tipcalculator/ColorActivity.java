package com.nmcginty97gmail.tipcalculator;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.widget.RadioGroup;

/**
 * Created by Nicholas on 9/25/2017.
 */


public class ColorActivity extends AppCompatActivity {

    private RadioGroup group;
    private int color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color);

        group = (RadioGroup) findViewById(R.id.group);

        int currentColor = getIntent().getIntExtra("CURRENT_COLOR", 0xFF00FFFF);
        System.out.println("CURRENT COLOR" + currentColor);
        // will check current color of background
        if(currentColor == 0xFF00FF00) {
            group.check(R.id.greenButton);
        } else if(currentColor == 0xFF00FFFF) {
            group.check(R.id.blueButton);
        } else if(currentColor == 0xFFFF00FF) {
            group.check(R.id.purpleButton);
        }

        Intent g = new Intent();
        g.putExtra("COLOR", currentColor);
        setResult(1, g);


        group.setOnCheckedChangeListener(
                new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                    if(i == R.id.greenButton) {
                        color = 0xFF00FF00;
                    } else if(i == R.id.blueButton) {
                        color = 0xFF00FFFF;
                    } else if(i == R.id.purpleButton) {
                        color = 0xFFFF00FF;
                    }
                    Intent g = new Intent();
                    g.putExtra("COLOR", color);
                    setResult(1, g);
                }
            }
        );
    }
}

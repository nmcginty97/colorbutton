package com.nmcginty97gmail.tipcalculator;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity {

    private EditText billEdit, tipDisplay;
    private TextView tipProgress;
    private SeekBar seekBar;
    int tipAmount = 20;
    private LinearLayout screen;
    private int color;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tipProgress = (TextView) findViewById(R.id.tipProgress);
        billEdit = (EditText) findViewById(R.id.billInput);
        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setMax(50);
        seekBar.setProgress(20);

        billEdit.setOnEditorActionListener(
                new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                        if(i == EditorInfo.IME_ACTION_DONE) { //i is id
                            double billAmount = Double.parseDouble(billEdit.getText().toString());
                            double totalAmount = billAmount * ((double) tipAmount / 100);
                            DecimalFormat df = new DecimalFormat("#.###");
                            Double d = new Double(totalAmount);


                            tipDisplay.setText("$" + df.format(d));
                            tipProgress.setText("" + tipAmount + "%");
                        }
                        return false;
                    }
                }
        );

        seekBar.setOnSeekBarChangeListener(
            new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                    if(billEdit.getText().toString().equals(""))
                        billEdit.setText("0");

                    tipAmount = i;
                    double billAmount = Double.parseDouble(billEdit.getText().toString());
                    double totalAmount = billAmount * ((double) tipAmount / 100);
                    DecimalFormat df = new DecimalFormat("#.###");
                    Double d = new Double(totalAmount);

                    tipDisplay.setText("$" + df.format(d));
                    tipProgress.setText("" + tipAmount + "%");

                    if(i < 5){
                        Context context = getApplicationContext();
                        CharSequence text = "Your server must've stunk!!";
                        int duration = Toast.LENGTH_SHORT;
                        Toast.makeText(context, text, duration).show();
                    }

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            }
        );

        tipDisplay = (EditText) findViewById(R.id.tipDisplay);
        screen = (LinearLayout) findViewById(R.id.screen);
        color = 0xFF00FFFF;
        screen.setBackgroundColor(color);
    }

    public void colorButtonPressed(View view) {
        Intent i = new Intent(this, ColorActivity.class);
        System.out.println("CURRENT_COLOR" + color);
        i.putExtra("CURRENT_COLOR", color);
        startActivityForResult(i, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        color = data.getIntExtra("COLOR", 0xFF00FFFF);
        screen.setBackgroundColor(color);
    }
}
